'use strict';

var request = require('superagent');
require('superagent-as-promised')(request);

function send() {
  request.post('http://example.com')
    .send({foo: 'wat', bar: {baz: [1,2,3,4,5]}})
    .then(function(res) {
      console.log('got a response');
    })
    .catch(function(err) {
      console.log('whoops');
    })
    ;
}

module.exports = function() {
  setInterval(send, 1000);
}
